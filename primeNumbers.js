/*
 * Enhanced Sieve of Erastosthenes algoritm
 * https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes
 * https://ru.wikipedia.org/wiki/%D0%A0%D0%B5%D1%88%D0%B5%D1%82%D0%BE_%D0%AD%D1%80%D0%B0%D1%82%D0%BE%D1%81%D1%84%D0%B5%D0%BD%D0%B0
 */
/*
  Отримати два числа, m і n. Вивести в консоль усі прості числа
  (http://ru.math.wikia.com/wiki/%D0%9F%D1%80%D0%BE%D1%81%D1%82%D0%BE%D0%B5_%D1%87%D0%B8%D1%81%D0%BB%D0%BE)
  в діапазоні від m до n (менше із введених чисел буде m, більше буде n).
  Якщо хоча б одне з чисел не відповідає умовам валідації, зазначеним вище,
  вивести повідомлення про помилку і запитати обидва числа знову.

*/
"use strict";
const isIntegerNumber = (number) => {
  return (typeof number === "string" && number.length) ||
    typeof number === "number"
    ? Number.isInteger(Number(number))
    : false;
};
function findPrimeNumbers(maxRange, minRange = 0) {
  if (minRange > maxRange) {
    [minRange, maxRange] = [maxRange, minRange];
  }
  if (maxRange <= 1) return [];
  const allNumbers = new Array(maxRange + 1).fill(true);
  allNumbers[0] = allNumbers[1] = false;
  for (let primeNumber = 2; primeNumber ** 2 <= maxRange; primeNumber++) {
    if (allNumbers[primeNumber]) {
      let notPrimeNumber;
      for (
        let quantifier = 0;
        (notPrimeNumber = primeNumber ** 2 + quantifier * primeNumber) <=
        maxRange;
        quantifier++
      ) {
        allNumbers[notPrimeNumber] = false;
      }
    }
  }
  const allPrimeNumbers = allNumbers.reduce(
    (allPrimeNumbers, isNumberPrime, primeNumber) => {
      if (isNumberPrime && primeNumber >= minRange && primeNumber <= maxRange)
        allPrimeNumbers.push(primeNumber);
      return allPrimeNumbers;
    },
    []
  );
  return allPrimeNumbers;
}

const getMinMaxRangeWithPrompt = () => {
  const maxRangeText = "Enter maximal number of range";
  const minRangeText = "Enter minimal number of range";
  const wrongRangeText =
    "Sorry, value of minimal or maximal bounds of range is not correct. Please enter it again";
  let minRange = "";
  let maxRange = "";
  while (true) {
    minRange = prompt(minRangeText, minRange);
    if (minRange === null) return null;
    maxRange = prompt(maxRangeText, maxRange);
    if (maxRange === null) return null;
    if (isIntegerNumber(minRange) && isIntegerNumber(maxRange)) {
      return [Number(minRange), Number(maxRange)];
    }
    alert(wrongRangeText);
  }
};
const startScript = () => {
  let minRange, maxRange;
  const minMaxRange = getMinMaxRangeWithPrompt();
  if (minMaxRange === null) return;
  [minRange, maxRange] = [...minMaxRange];
  const primeNumbers = findPrimeNumbers(maxRange, minRange);
  console.log(primeNumbers.join(", "));
  // console.log(
  //   `Prime numbers in range from ${minRange} to ${maxRange}:\n\n${primeNumbers.join(
  //     ", "
  //   )}`
  // );
};

startScript();

// const startButton = document.getElementById("start_button");
// startButton.addEventListener("click", startScript);

// const testCases = () => {
//   console.log("findPrimeNumbers(-100) must be []");
//   console.log(findPrimeNumbers(-100));
//   console.log("findPrimeNumbers(-100,100) lenght must be 25");
//   console.log(findPrimeNumbers(-100, 100));
//   console.log("findPrimeNumbers(100,-100) lenght must be 25");
//   console.log(findPrimeNumbers(100, -100));
//   console.log("findPrimeNumbers(0) must be []");
//   console.log(findPrimeNumbers(0));
//   console.log("findPrimeNumbers(1) must be []");
//   console.log(findPrimeNumbers(1));
//   console.log("findPrimeNumbers(2) must be [2]");
//   console.log(findPrimeNumbers(2));
//   console.log("findPrimeNumbers(200) lenght must be 46");
//   console.log(findPrimeNumbers(200));
//   console.log("findPrimeNumbers(7917) length must be 999");
//   console.log(findPrimeNumbers(7917));
//   console.log("findPrimeNumbers(100, 8117); length must be 996");
//   console.log(findPrimeNumbers(100, 8117));
// };
//testCases();
