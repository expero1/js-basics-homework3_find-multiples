# Basic Javascript Homework 3 DAN IT 

1. Описати своїми словами у кілька рядків, навіщо у програмуванні потрібні цикли.

Цикли в програмуванні потрібні для того, щоб можна було виконати однитипну послідовність дій декілька раз
Окрім цього цикли можуть використовуватися для виконання певних дії до тих пір, поки не буде виконана задана умова виконання циклу.


2. Опишіть у яких ситуаціях ви використовували той чи інший цикл в JS.

Для того, щоб вивести на веб-сторінку дані, отримані з серверу у вигляді набору даних в JSON. Можна використати цикл `for` і послідовно додати ці дані в DOM.

Для отримання даних за допомогою prompt та перевірки іх на корректність, я використовував нескінченний цикл `while(true)`, а для виходу з циклу використовув перевірку на null (коли натиснута кнопка 'Cancel' в prompt) або після валідації ведених даних повертаються значення.

В скрипті знаходження простих чисел використав цикл `for` з умовою виходу з циклу, якщо число, що перевіряється більше за максимальний діапазан розрахунку.




3. Що таке явне та неявне приведення (перетворення) типів даних у JS?
   
Перетворення типів даних в Javascript це особливість мови програмування, яка дозволяє змінити тип даних з одного на інший.

Явне перетворення типів відбувається, якщо викликати відповідну функцію Javascript. Наприклад `Number('123')` сбробує перетворити рядок `'123'` на відповідне число і поверне `123`. Таким самим чином можна використовувати `String(value)` для перетворення даних в текстовий рядок, `Boolean(value)` для перетворення до булевого типу.

Також в Javascript є механізм неявного перетворення типів даних. Він застосовується у випадках, коли функція або операція очікує один тип даних, але отримує інший. В такому випадку Javscript намагається перетворити один тип даних на такий, який потрібен в даному випадку. Наприклад функція `if` очікує булевий тип, але якщо замість цього отримує, наприклад, текстовий рядок, він буде перетворений на логічне значення - непустий рядок перетвориться на `true`, а пустий - на `false`.

