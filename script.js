/*
  Реалізувати програму на Javascript, яка знаходитиме всі числа кратні 5 (діляться на 5 без залишку)
  у заданому діапазоні. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

  Технічні вимоги:

  - Отримати за допомогою модального вікна браузера число, яке введе користувач.
  - Вивести в консолі всі числа, кратні 5, від 0 до введеного користувачем числа.
    Якщо таких чисел немає - вивести в консоль фразу "Sorry, no numbers"
  - Обов'язково необхідно використовувати синтаксис ES6 (ES2015) для створення змінних.

  Необов'язкове завдання підвищеної складності

  - Перевірити, чи введене значення є цілим числом.
    Якщо ця умова не дотримується, повторювати виведення вікна на екран, доки не буде введено ціле число.
  - Отримати два числа, m і n. Вивести в консоль усі прості числа
    (http://ru.math.wikia.com/wiki/%D0%9F%D1%80%D0%BE%D1%81%D1%82%D0%BE%D0%B5_%D1%87%D0%B8%D1%81%D0%BB%D0%BE)
    в діапазоні від m до n (менше із введених чисел буде m, більше буде n).
    Якщо хоча б одне з чисел не відповідає умовам валідації, зазначеним вище,
    вивести повідомлення про помилку і запитати обидва числа знову.
*/
const maxNumberText = "Enter maximal number of range";
const noNumbersText = "Sorry, no numbers";
const checkNumberIsInteger = (number) => {
  return (typeof number === "string" && number.length) ||
    typeof number === "number"
    ? Number.isInteger(Number(number))
    : false;
};
function getNumberWithPrompt(promptText) {
  let number = "";
  while (true) {
    number = prompt(promptText, number);
    if (number === null) return number;
    if (checkNumberIsInteger(number)) {
      return Number(number);
    }
  }
}
function findMultipleNumbers(multiplier, minRange, maxRange) {
  const numbersAccumulator = [];
  if (minRange > maxRange) {
    [minRange, maxRange] = [maxRange, minRange];
  }
  let currentNumber = minRange;
  while (maxRange >= currentNumber) {
    if (currentNumber % multiplier === 0) {
      if (currentNumber !== 0) numbersAccumulator.push(currentNumber);
      currentNumber += multiplier;
      continue;
    }
    currentNumber += 1;
  }
  return numbersAccumulator;
}

const getMultipliers = () => {
  const multiplier = 5;
  const minRange = 0;
  const maxRange = getNumberWithPrompt(maxNumberText);
  if (maxRange === null) return null;
  const multipleNumbers = findMultipleNumbers(multiplier, minRange, maxRange);
  return multipleNumbers;
};
startScript = () => {
  const multipleNumbers = getMultipliers();
  if (multipleNumbers === null) return;
  multipleNumbers.length
    ? console.log(multipleNumbers.join(", "))
    : console.log(noNumbersText);
};

startScript();
// const startButton = document.getElementById("start_button");
// startButton.addEventListener("click", startScript);

// const checkNumberIsIntegerTestCases = () => {
//   console.log(checkNumberIsInteger(10));
//   console.log(checkNumberIsInteger("10"));
//   console.log(checkNumberIsInteger("true"));
//   console.log(checkNumberIsInteger("asd"));
//   console.log(checkNumberIsInteger(""));
//   console.log(checkNumberIsInteger("true"));
// };

//checkNumberIsIntegerTestCases();
// const testCases = () => {
//   console.log("findMultipleNumbers(5, 0, 56)");
//   console.log(findMultipleNumbers(5, 0, 56).join(", "));
//   console.log("findMultipleNumbers(5, 0, -56)");
//   console.log(findMultipleNumbers(5, 0, -56).join(", "));
//   console.log("findMultipleNumbers(5, -56, 56)");
//   console.log(findMultipleNumbers(5, -56, 56).join(", "));
//   console.log("findMultipleNumbers(5, 56, -56)");
//   console.log(findMultipleNumbers(5, 56, -56).join(", "));
//   console.log("findMultipleNumbers(5, 4, -4)");
//   console.log(findMultipleNumbers(5, 4, -4).join(", "));
//   console.log("findMultipleNumbers(5, -4, 4)");
//   console.log(findMultipleNumbers(5, -4, 4).join(", "));
// };
//testCases();
